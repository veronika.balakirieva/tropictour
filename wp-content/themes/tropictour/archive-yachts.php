<?php get_header() ?>
    <?php get_template_part('template-parts/layout/banner') ?>

    <div class="list-wrapper bg-grey-2">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="sidebar bg-white clearfix">

                        <div class="sidebar-block">
                            <h4 class="sidebar-title color-dark-2">Категории</h4>
                            <ul class="sidebar-category color-3">
                                <?php foreach (getCategoriesArray('yachts-categories') as $i => $category) : ?>
                                    <li <?php if (! $i) echo 'class="active"' ?>>
                                        <a class="cat-drop" href="<?= get_term_link($category['id'], 'yachts-categories') ?>">
                                            <?= $category['name'] ?>
                                            <span class="fr">(<?= $category['count'] ?>)</span>
                                        </a>
                                        <?php if (count($category['kids'])) : ?>
                                            <ul>
                                                <?php foreach ($category['kids'] as $kid) : ?>
                                                    <li>
                                                        <a href="<?= get_term_link($kid['id'], 'yachts-categories') ?>">
                                                            <?= $kid['name'] ?> (<?= $kid['count'] ?>)
                                                        </a>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9">
                    <div class="grid-content clearfix">
                        <?php if (have_posts()) : while (have_posts()) : the_post() ?>
                            <div class="list-item-entry">
                                <div class="hotel-item style-9 bg-white yacht-item">
                                    <div class="table-view">
                                        <div class="radius-top cell-view">
                                            <div class="block-item-img" style="background-image: url('<?= getPostThumbnailUrl() ?>')"></div>
                                           <!--  <img src="<?= getPostThumbnailUrl() ?>"
                                                 alt=""> -->
                                        </div>
                                        <div class="title hotel-middle cell-view yacht-desc">
                                            <div class="tour-info-line clearfix">
                                                <div class="tour-info fl">
                                                    <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png"
                                                         alt="">
                                                    <span class="font-style-2 color-grey-3"><?= get_the_date("d.m.Y") ?></span>
                                                </div>
                                            </div>
                                            <h4><b><?php the_title() ?></b></h4>
                                            <p class="f-14 color-grey-3"><?= get_field('excerpt') ?></p>
                                            <a href="<?php the_permalink() ?>" class="c-button b-40 bg-aqua">
                                                <span>Перейти</span>
                                            </a>
                                        </div>
                                        <div class="title hotel-right clearfix cell-view grid-hidden">
                                            <div class="rate-wrap">
                                                <i>485 rewies</i>
                                                <div class="rate">
                                                    <span class="fa fa-star color-yellow"></span>
                                                    <span class="fa fa-star color-yellow"></span>
                                                    <span class="fa fa-star color-yellow"></span>
                                                    <span class="fa fa-star color-yellow"></span>
                                                    <span class="fa fa-star color-yellow"></span>
                                                </div>
                                            </div>
                                            <div class="hotel-person color-dark-2">from <span
                                                        class="color-blue">$755</span> person
                                            </div>
                                            <img class="hotel-right-logo"
                                                 src="/wp-content/themes/tropictour/img/tour_list/cruise_logo_1.jpg"
                                                 alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; else : ?>
                            <h3>По вашему запросу увы ничего не найдено</h3>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>