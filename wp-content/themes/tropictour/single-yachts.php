<?php get_header();
while (have_posts()) : the_post() ?>
    <?php get_template_part('template-parts/layout/banner') ?>
    <div class="detail-wrapper">
        <div class="container">
            <div class="detail-header">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <h2 class="detail-title color-dark-2"><?php the_title() ?></h2>
                    </div>
<!--                    <div class="col-xs-12 col-sm-4">-->
<!--                        <div class="detail-price color-dark-2">price from <span class="color-dr-blue"> $200</span>-->
<!--                            /person-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="row padd-90">
                <div class="col-xs-12 col-md-8">
                    <div class="detail-content">
                        <div class="detail-top">
                            <?php get_template_part('template-parts/layout/gallery') ?>
                        </div>
                        <?php the_content() ?>
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="right-sidebar">
                        <?php get_template_part('template-parts/layout/help') ?>
                        <div class="form-wrap yaht-form-wrap">
                            <div class="form-title color-dark-2">Оставить заявку</div>
                            <form action="" id="yaht-order-form" class="yaht-order-form">
                                <div class="form-message">
                                    <h4></h4>
                                </div>
                                <div class="form-row input-row input-style-1 type-2 color-2">
                                    <input type="date">
                                </div>
                                <div class="form-row input-row input-style-1 type-2 color-2">
                                    <input type="text" required placeholder="Название услуги">
                                </div>
                                <div class="form-row input-row input-style-1 type-2 color-2">
                                    <input type="text" required placeholder="Имя">
                                </div>
                                <div class="form-row input-row input-style-1 type-2 color-2">
                                    <input type="text" required placeholder="Номер телефона">
                                </div>
                                <div class="form-row input-row input-style-1 type-2 color-2">
                                    <input type="text" required placeholder="Email">
                                </div>
                                <div class="form-row input-row area-row">
                                    <textarea name="yaht-comment" id="yaht-comment" class="area-style-1 color-1"
                                              required placeholder="Комментарий"></textarea>
                                </div>
                                <div class="form-row btn-row">
                                    <button type="submit" class="c-button bg-aqua"><span>Оставить заявку</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php $query = getRelatedYachts($post); if ($query->have_posts()) : ?>
                <div class="may-interested padd-90">
                    <div class="row">
                        <?php while ($query->have_posts()) : $query->the_post() ?>
                            <div class="col-mob-12 col-xs-6 col-sm-6 col-md-3">
                                <div class="hotel-item style-6">
                                    <div class="radius-top">
                                        <div class="block-item-img" style="background-image: url('<?= getPostThumbnailUrl() ?>')"></div>
                                       <!--  <img src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>"> -->
                                    </div>
                                    <div class="title">
                                        <div class="tour-info-line clearfix">
                                            <div class="tour-info fl">
                                                <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                                <span class="font-style-2 color-grey-3"><?= get_the_date("d.m.Y") ?></span>
                                            </div>
                                        </div>
                                        <h4><b><?php the_title() ?></b></h4>
                                        <p class="f-14 color-grey-3"><?= get_field('excerpt') ?></p>
                                        <a href="<?= get_the_permalink() ?>" class="c-button b-50 bg-grey-3-t1 hv-grey-3-t">Перейти</a>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php endwhile;
get_footer() ?>