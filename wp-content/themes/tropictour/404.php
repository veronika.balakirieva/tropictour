<?php get_header() ?>
<div class="not-found">
    <img class="center-image" src="/wp-content/themes/tropictour/img/special/bg-2.jpg" alt="">
    <div class="not-found-box">
        <div class="not-found-title">ouch!</div>
        <div class="not-found-message">sorry the page you are looking for does not exist</div>
    </div>
</div>
<?php get_footer() ?>