<?php get_header() ?>

<?php get_template_part('template-parts/layout/banner') ?>

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-8">
                <div class="blog-list">
                    <?php if (have_posts()) : while (have_posts()) : the_post() ?>
                        <div class="blog-list-entry">
                            <div class="blog-list-top block-item-img" style="background-image: url('<?= getPostThumbnailUrl() ?>')">
<!--                                <img class="img-responsive" src="--><?//= getPostThumbnailUrl() ?><!--"-->
<!--                                     alt="--><?php //the_title() ?><!--">-->
                            </div>
                            <h4 class="blog-list-title">
                                <a class="color-dark-2 link-dr-blue-2" href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </h4>
                            <div class="tour-info-line clearfix">
                                <div class="tour-info fl">
                                    <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                    <span class="font-style-2 color-dark-2"><?= get_the_date("d.m.Y") ?></span>
                                </div>
                            </div>
                            <div class="blog-list-text color-grey-3"><?= get_field('excerpt') ?></div>
                            <a href="<?php the_permalink() ?>" class="c-button small bg-aqua">
                                <span>Перейти</span>
                            </a>
                        </div>
                    <?php endwhile; else : ?>
                        <h2>Пока что нет ни одной записи</h2>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="right-sidebar">
                    <div class="sidebar-block type-2">
                        <h4 class="sidebar-title color-dark-2"><?= trans('Категории', 'Categories') ?></h4>
                        <ul class="sidebar-category color-5">
                            <?php foreach (getCategoriesArray('posts-categories') as $i => $category) : ?>
                                <li>
                                    <a href="<?= get_term_link($category['id'], 'posts-categories') ?>">
                                        <?= $category['name'] ?><span class="fr">(<?= $category['count'] ?>)</span>
                                    </a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <div class="sidebar-block type-2">
                        <h4 class="sidebar-title color-dark-2"><?= trans('Теги', 'Tags') ?></h4>
                        <ul class="widget-tags clearfix">
                            <?php foreach (getCategoriesArray('posts-tags') as $i => $category) : ?>
                                <li>
                                    <a class="c-button b-30 b-1 bg-grey-2 hv-dr-blue-2"
                                       href="<?= get_term_link($category['id'], 'posts-tags') ?>">
                                        <?= $category['name'] ?>
                                    </a>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer() ?>				   