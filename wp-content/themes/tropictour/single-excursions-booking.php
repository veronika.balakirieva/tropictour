<?php get_header(); while (have_posts()) : the_post() ?>
    <div class="detail-wrapper">
        <div class="container">
            <div class="row padd-90">
                <div class="col-xs-12 col-md-8">
                    <form class="simple-from" method="post">
                        <div class="simple-group">
                            <h3 class="small-title">Бронирование экскурсии "<?= the_title() ?>"</h3>
                            <div class="row">

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Имя', 'First Name') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="text" placeholder="<?= trans('Введите Ваше имя', 'Enter your first name') ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Фамилия', 'Last Name') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="text" placeholder="<?= trans('Введите Вашу фамилию', 'Enter your last name') ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Электронная почта', 'E-mail Address') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="text" placeholder="<?= trans('Введите Ваш e-mail адрес', 'Enter your e-mail address') ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Номер телефона', 'Phone number') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="text" placeholder="<?= trans('Введите Ваш номер телефона', 'Enter your phone number') ?>">
                                        </div>
                                    </div>
                                </div>

                                <?php if ($programs = get_field('programs')) : ?>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-block type-2 clearfix">
                                            <div class="form-label color-dark-2"><?= trans('Выберите программу', 'Chose your program') ?></div>
                                            <div class="drop-wrap drop-wrap-s-4 color-5">
                                                <div class="drop disabled">
                                                    <b><?= $programs[0]['title'] ?></b>
                                                    <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                                    <span>
                                                        <?php foreach ($programs as $program) : ?>
                                                            <a href="#"
                                                               data-price-1="<?= $program['price-1-sale'] ?: $program['price-1'] ?>"
                                                               data-price-2="<?= $program['price-2-sale'] ?: $program['price-2'] ?>">
                                                                <?= $program['title'] ?>
                                                            </a>
                                                        <?php endforeach ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <?php $districts = getDistrictsQuery(); if ($districts->have_posts()) : ?>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-block type-2 clearfix">
                                            <div class="form-label color-dark-2"><?= trans('Выберите программу', 'Chose your program') ?></div>
                                            <div class="drop-wrap drop-wrap-s-4 color-5">
                                                <div class="drop disabled">
                                                    <b><?= $districts->posts[0]->post_title ?></b>
                                                    <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                                    <span>
                                                        <?php foreach ($districts->posts as $district) : ?>
                                                            <a href="#"
                                                               data-price="<?= get_field('district-price-' . $district->ID) ?>">
                                                                <?= $district->post_title ?>
                                                            </a>
                                                        <?php endforeach ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Количество взрослых', 'Adults') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="number" value="1" min="1">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Количество детей (до 12 лет)', 'Kids (up to 12 y.o.)') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="number" value="0" min="0">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-block type-2 clearfix">
                                        <div class="form-label color-dark-2"><?= trans('Количество младенцев (до 4 лет)', 'Babies (up to 4 y.o.)') ?></div>
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="number" value="0" min="0">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="confirm-terms">
                                        <div class="input-entry color-3">
                                            <input class="checkbox-form" id="text-1" type="checkbox" name="checkbox" value="climat control">
                                            <label class="clearfix" for="text-1">
                                                <span class="sp-check"><i class="fa fa-check"></i></span>
                                                <span class="checkbox-text">
                                                    <?= trans(
                                                            'Я прочитал и принимаю правила <a class="color-dr-blue-2 link-dark-2" target="_blank" href="/policies/">Публичной офферты</a>',
                                                            'Я прочитал и принимаю правила <a class="color-dr-blue-2 link-dark-2" target="_blank" href="/policies/">Публичной офферты</a>'
                                                    ) ?>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="<?= trans('Забронировать', 'Book this excursion') ?>">
                        <input type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o" value="<?= trans('Оплатить', 'Pay') ?> (Paypal)">
                    </form>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="right-sidebar">
                        <?php get_template_part('template-parts/layout/help') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; get_footer() ?>