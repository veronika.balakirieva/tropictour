<footer class="bg-dark type-2">
    <div class="footer-link bg-black">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>&copy; 2018. Все права защищены. Разработка и поддержка <a href="http://melon.team/" target="_blank">Melon D&D</a></p>
                    </div>
                    <ul>
                        <?php foreach (getMenuArray("footer") as $item) : ?>
                            <li><a class="link-aqua" href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="/wp-content/themes/tropictour/js/jquery-2.1.4.min.js"></script>
<script src="/wp-content/themes/tropictour/js/bootstrap.min.js"></script>
<script src="/wp-content/themes/tropictour/js/jquery-ui.min.js"></script>
<script src="/wp-content/themes/tropictour/js/idangerous.swiper.min.js"></script>
<script src="/wp-content/themes/tropictour/js/jquery.viewportchecker.min.js"></script>
<script src="/wp-content/themes/tropictour/js/isotope.pkgd.min.js"></script>
<script src="/wp-content/themes/tropictour/js/jquery.mousewheel.min.js"></script>
<script src="/wp-content/themes/tropictour/js/all.js"></script>
<script src="/wp-content/themes/tropictour/js/olga.js"></script>
</body>
</html>