<?php get_header(); while (have_posts()) : the_post() ?>
<?php get_template_part('template-parts/layout/banner') ?>
<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-8">
                <div class="detail-header style-2">
                    <h2 class="detail-title color-dark-2"><?php the_title() ?></h2>
                    <div class="tour-info-line clearfix">
                        <div class="tour-info fl">
                            <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                            <span class="font-style-2 color-dark-2"><?= get_the_date("d.m.Y") ?></span>
                        </div>
                    </div>
                </div>
                <div class="detail-content">
                    <div class="detail-content-block">
                        <?php get_template_part('template-parts/layout/gallery') ?>
                        <?php the_content() ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="right-sidebar">
                    <?php get_template_part('template-parts/layout/help') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; get_footer() ?>