<!DOCTYPE html>
<html>
<head>
    <?php get_template_part('template-parts/layout/head-meta') ?>
</head>
<body data-color="theme-1">

<?php get_template_part('template-parts/layout/loader') ?>

<header class="color-1 hovered menu-3">
    <!-- <div class="container"> -->
        <div class="row">
            <div class="col-md-12">
                   <div class="top-header-bar">
        <div class="container">
            <div class="left-col">
                <a href="tel:<?= get_field('phone', CONTACT_PAGE_ID) ?>">
                    <i class="fa fa-phone"></i><?= forceTrans(get_field('phone', CONTACT_PAGE_ID)) ?>
                </a>
                <a href="mailto:<?= get_field('email', CONTACT_PAGE_ID) ?>">
                    <i class="fa fa-envelope"></i><?= forceTrans(get_field('email', CONTACT_PAGE_ID)) ?>
                </a>
            </div>
            <div class="right-col">
                <div class="folow">
                    <a href="<?= forceTrans(get_field('facebook', CONTACT_PAGE_ID)) ?>" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="<?= forceTrans(get_field('instagram', CONTACT_PAGE_ID)) ?>" target="_blank">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="<?= forceTrans(get_field('vk', CONTACT_PAGE_ID)) ?>" target="_blank">
                        <i class="fa fa-vk"></i>
                    </a>
                    <a href="<?= forceTrans(get_field('youtube', CONTACT_PAGE_ID)) ?>" target="_blank">
                        <i class="fa fa-youtube"></i>
                    </a>
                </div>
                <?php get_template_part('template-parts/layout/language-switcher') ?>
            </div>
        </div>
    </div>
                <div class="nav">
                    <div class="container">
          <a href="/" class="logo"><span>Tropic</span>
                        <img src="/wp-content/themes/tropictour/img/theme-1/logo.svg" alt="tropic tour">
                        <span>Tour</span>
                    </a>
                    <div class="header-nav-wrap">
                        <div class="nav-menu-icon">
                            <a href="#"><i></i></a>
                        </div>
                        <?php get_template_part('template-parts/layout/dropdown-menu') ?>
                        <!-- <?php get_template_part('template-parts/layout/language-switcher') ?> -->
                    </div>
                    </div>

                </div>
            </div>
        </div>
    <!-- </div> -->
</header>