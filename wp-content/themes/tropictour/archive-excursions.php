<?php get_header() ?>

<?php get_template_part('template-parts/layout/banner') ?>

<div class="main-wraper bg-grey-2 padd-90">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="second-title">
                    <h4 class="subtitle color-red underline">Наши экскурсии</h4>
                    <h2>Мы предлагаем Вам</h2>
                </div>
            </div>
        </div>
        <div class="tour-item-grid row">
            <?php if (have_posts()) : while (have_posts()) : the_post() ?>
                <div class="col-mob-12 col-xs-6 col-sm-6 col-md-4 tour-item-wrap">
                    <div class="tour-item style-5">
                        <div class="radius-top">
                            <div class="block-item-img" style="background-image: url(<?= getPostThumbnailUrl() ?>"></div>
                        </div>
                        <div class="tour-desc bg-white">
                            <h4>
                                <a class="tour-title color-dark-2 link-dr-blue-2" href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </h4>
                            <div class="tour-text color-grey-3"><?= get_field('excerpt') ?></div>
                            <?php if ($price = get_field('price')) : ?>
                                <div class="tour-person">
                                    from <span><?= $price ?></span>
                                </div>
                            <?php endif ?>
                            <div class="title-bottom">
                                <a href="<?php the_permalink() ?>" class="c-button bg-aqua"><span>Перейти</span></a>
                                <?php get_template_part('template-parts/excursions/price') ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; else : ?>
                <h3>По вашему запросу увы ничего не найдено</h3>
            <?php endif ?>
        </div>
    </div>
</div>

<?php get_footer() ?>
