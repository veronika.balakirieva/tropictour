<?php get_header(); while (have_posts()) : the_post() ?>
    <?php get_template_part('template-parts/layout/banner') ?>
    <div class="detail-wrapper">
        <div class="container">
            <div class="row padd-90">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="additional-block padd-90">
                        <ul class="comments-block">
                            <li class="comment-entry clearfix">
                                <div class="commnent-img" style="background-image: url('/wp-content/themes/tropictour/img/detail/comment_1.jpg')"></div>
                                <!-- <img class="commnent-img" src="/wp-content/themes/tropictour/img/detail/comment_1.jpg" alt=""> -->
                                <div class="comment-content clearfix">
                                    <div class="tour-info-line">
                                        <div class="tour-info">
                                            <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                            <span class="font-style-2 color-dark-2">03/07/2015</span>
                                        </div>
                                        <div class="tour-info">
                                            <img src="/wp-content/themes/tropictour/img/people_icon_grey.png" alt="">
                                            <span class="font-style-2 color-dark-2">By Emma Stone</span>
                                        </div>
                                    </div>
                                    <div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
                                    <a class="comment-reply c-button b-26 bg-aqua" href="#"><span>Reply</span></a>
                                </div>
                                <ul class="comments-block">
                                    <li class="comment-entry clearfix">
                                        <div class="commnent-img" style="background-image: url('/wp-content/themes/tropictour/img/detail/comment_2.jpg')"></div>
                                        <!-- <img class="commnent-img" src="/wp-content/themes/tropictour/img/detail/comment_2.jpg" alt=""> -->
                                        <div class="comment-content clearfix">
                                            <div class="tour-info-line">
                                                <div class="tour-info">
                                                    <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                                    <span class="font-style-2 color-dark-2">03/07/2015</span>
                                                </div>
                                                <div class="tour-info">
                                                    <img src="/wp-content/themes/tropictour/img/people_icon_grey.png" alt="">
                                                    <span class="font-style-2 color-dark-2">By Emma Stone</span>
                                                </div>
                                            </div>
                                            <div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
                                            <a class="comment-reply c-button b-26 bg-aqua" href="#"><span>Reply</span></a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="comment-entry clearfix">
                                <div class="commnent-img" style="background-image: url('/wp-content/themes/tropictour/img/detail/comment_3.jpg')"></div>
                                <!-- <img class="commnent-img" src="/wp-content/themes/tropictour/img/detail/comment_3.jpg" alt=""> -->
                                <div class="comment-content clearfix">
                                    <div class="tour-info-line">
                                        <div class="tour-info">
                                            <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                            <span class="font-style-2 color-dark-2">03/07/2015</span>
                                        </div>
                                        <div class="tour-info">
                                            <img src="/wp-content/themes/tropictour/img/people_icon_grey.png" alt="">
                                            <span class="font-style-2 color-dark-2">By Emma Stone</span>
                                        </div>
                                    </div>
                                    <div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
                                    <a class="comment-reply c-button b-26 bg-aqua" href="#"><span>Reply</span></a>
                                </div>
                            </li>
                            <li class="comment-entry clearfix">
                                <div class="commnent-img" style="background-image: url('/wp-content/themes/tropictour/img/detail/comment_1.jpg')"></div>
                                <!-- <img class="commnent-img" src="/wp-content/themes/tropictour/img/detail/comment_1.jpg" alt=""> -->
                                <div class="comment-content clearfix">
                                    <div class="tour-info-line">
                                        <div class="tour-info">
                                            <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                            <span class="font-style-2 color-dark-2">03/07/2015</span>
                                        </div>
                                        <div class="tour-info">
                                            <img src="/wp-content/themes/tropictour/img/people_icon_grey.png" alt="">
                                            <span class="font-style-2 color-dark-2">By Emma Stone</span>
                                        </div>
                                    </div>
                                    <div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
                                    <a class="comment-reply c-button b-26 bg-aqua" href="#"><span>Reply</span></a>
                                </div>
                            </li>
                        </ul>
                        <form>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="text" required="" placeholder="Enter your name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-block type-2 clearfix">
                                        <div class="input-style-1 b-50 brd-0 type-2 color-3">
                                            <input type="text" required="" placeholder="Enter your email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 text-center">
                                    <div class="form-block type-2 clearfix">
                                        <textarea class="area-style-1 type-2 color-3" required="" placeholder="Write a comment..."></textarea>
                                    </div>
                                    <button type="submit" class="c-button b-40 bg-aqua"><span>post comment</span></button>
                                    <!-- <input type="submit"  value="post comment"> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; get_footer() ?>