<?php get_header('home') ?>
<?php if ($slides = get_field('slider')) : ?>
    <div class="full-height">
        <div class="top-baner swiper-animate arrows">
            <div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900"
                 data-center="0"
                 data-slides-per-view="1">
                <div class="swiper-wrapper">
                    <?php foreach ($slides as $slide) : ?>
                        <div class="swiper-slide active" data-val="0">
                            <div class="clip">
                                <div class="tour-layer"></div>
                                <div class="bg bg-bg-chrome act"
                                     style="background-image:url(<?= getImageUrl($slide[ 'image' ], 'large') ?>)">
                                </div>
                            </div>
                            <div class="vertical-align">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="main-title vert-title">
                                                <h1 class="color-white delay-1"><?= forceTrans($slide[ 'title' ]) ?></h1>
                                                <p class="color-white-op delay-2"><?= forceTrans($slide[ 'desctription' ]) ?></p>
                                                <?php if ($slide[ 'url-title' ] && $slide[ 'url-href' ]) : ?>
                                                    <a href="<?= forceTrans($slide[ 'url-href' ]) ?>"
                                                       class="c-button bg-aqua hv-transparent delay-2">
                                                        <span><?= forceTrans($slide[ 'url-title' ]) ?></span>
                                                    </a>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
                <div class="pagination pagination-hidden poin-style-1"></div>
            </div>
            <div class="arrow-wrapp m-200">
                <div class="cont-1170">
                    <div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
                    <div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($excursions = get_field('excursions')) : ?>
    <div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>Экскурсии</h2>
                        <p class="color-grey"><?= forceTrans(get_field('above-excursions')) ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($excursions as $excursion) : $term = get_term($excursion['id'], 'excursion-categories') ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="radius-mask tour-block hover-aqua">
                            <div class="clip">
                                <div class="bg bg-bg-chrome act"
                                     style="background-image:url(<?= getImageUrl(carbon_get_term_meta($term->term_id, 'image')) ?>)">
                                </div>
                            </div>
                            <div class="tour-layer delay-1"></div>
                            <div class="tour-caption">
                                <div class="vertical-align">
                                    <h3 class="hover-it"><?= $term->name ?></h3>
                                </div>
                                <div class="vertical-bottom">
                                    <a href="<?= get_term_link($term->term_id, 'excursion-categories') ?>" class="c-button b-50 bg-aqua hv-transparent fr">
                                        <span>Перейти</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata() ?>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($excursions = get_field('top-excursions')) : ?>
    <div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h2>Топ экскурсий на пхукете</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($excursions as $excursion) : $post = get_post($excursion['id']) ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="hotel-item style-5">
                            <div class="radius-top">
                                <div class="block-item-img" style="background-image: url('<?= getPostThumbnailUrl() ?>')"></div>
                                <!-- <img src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>"> -->
                                <div class="price price-s-2 red tt">hot price</div>
                            </div>
                            <div class="title">
                                <h4><b><?php the_title() ?></b></h4>
                                <p class="f-14"><?= get_field('excerpt') ?></p>


                                <!--TODO-->
                                <!-- <div class="c-price fr">$600</div> -->
                            </div>
                            <div class="title-bottom">
                                 <a href="<?php the_permalink() ?>" class="c-button bg-aqua"><span>Перейти</span></a>
                                <?php get_template_part('template-parts/excursions/price') ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata() ?>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($excursions = get_field('popular-excursions')) : ?>
    <div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h2>Популярные экскурсии</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($excursions as $excursion) : $post = get_post($excursion['id']) ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="hotel-small style-2 clearfix">
                            <a class="hotel-img black-hover" href="<?php the_permalink() ?>">
                                <div class="block-item-img radius-3" style="background-image: url('<?= getPostThumbnailUrl() ?>')"></div>
                                <!-- <img class="img-responsive radius-3"
                                     src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>"> -->
                                <div class="tour-layer delay-1 radius-3"></div>
                            </a>
                            <div class="hotel-desc">
                                <h4><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h4>
                            </div>
                            <?php get_template_part('template-parts/excursions/price') ?>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata() ?>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($services = get_field('services')) : ?>
    <div class="main-wraper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>Услуги</h2>
                        <p class="color-grey"><?= forceTrans(get_field('above-services')) ?></p>
                    </div>
                </div>
            </div>
            <div class="row col-no-padd">
                <?php foreach ($services as $service) : $post = get_post($service['id']) ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="photo-block hover-aqua">
                            <div class="tour-layer delay-1"></div>
                            <div class="block-item-img pb-53" style="background-image:url('<?= getPostThumbnailUrl() ?>')"></div>
                            <div class="vertical-align">
                                <div class="photo-title">
                                    <a class="hover-it" href="<?= get_the_permalink() ?>">
                                        <h3><?php the_title() ?></h3>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata() ?>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($yachts = get_field('yachts')) : ?>
    <div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>Аренда яхт</h2>
                        <p class="color-grey"><?= forceTrans(get_field('above-yachts')) ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="offers-slider-wrap arrows">
                    <div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500"
                         data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2"
                         data-md-slides="3" data-lg-slides="3" data-add-slides="3">
                        <div class="swiper-wrapper">
                            <?php foreach ($yachts as $yacht) : $post = get_post($yacht['id']) ?>
                                <div class="swiper-slide" data-val="0">
                                    <div class="offers-block radius-mask">
                                        <div class="clip">
                                            <div class="bg bg-bg-chrome act"
                                                 style="background-image:url(<?= getPostThumbnailUrl() ?>)">
                                            </div>
                                        </div>
                                        <div class="offers-top">
                                            <div class="offers-title">
                                                <h3><?php the_title() ?></h3>
                                            </div>
                                        </div>
                                       
                                        <div class="tour-layer delay-1"></div>
                                        <div class="vertical-bottom">
                                            <p><?php get_field('excerpt') ?></p>
                                            <a href="<?php the_permalink() ?>" class="c-button bg-aqua hv-aqua-o b-40">
                                                <span>Перейти</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; wp_reset_postdata() ?>
                        </div>
                        <div class="pagination  poin-style-1 pagination-hidden"></div>
                    </div>
                    <div class="swiper-arrow-left offers-arrow"><span class="fa fa-angle-left"></span></div>
                    <div class="swiper-arrow-right offers-arrow"><span class="fa fa-angle-right"></span></div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($posts = get_field('posts')) : ?>
    <div class="main-wraper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h2>Актуальное по теме</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($posts as $post) : $post = get_post($post['id']) ?>
                    <div class="col-xs-12 col-sm-4">
                        <div class="s_news-entry">
                            <div class="block-item-img s_news-img" style="background-image:url('<?= getPostThumbnailUrl() ?>')"></div>
                            <!-- <img class="s_news-img img-full img-responsive"
                                 src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>"> -->
                            <h4 class="s_news-title">
                                <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                            </h4>
                            <div class="s_news-text color-grey-3"><?= get_field('excerpt') ?></div>
                        </div>
                    </div>
                <?php endforeach; wp_reset_postdata() ?>
            </div>
            <div class="row btn-row articles-btn-row">
                <a href="#" class="c-button b-50 bg-aqua hv-transparent fr">
                    <span>Все статьи</span>
                </a>
            </div>
        </div>
    </div>
<?php endif ?>
    <div class="main-wraper padd-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="second-title">
                        <h2>О компании</h2>
                        <p class="color-grey"><?= forceTrans(get_field('about-excerpt')) ?></p>
                    </div>
                </div>
            </div>
            <div class="row about-row">
                <div class="col-xs-12 col-md-6 col-md-push-6 col-sm-12">
                    <div class="popular-desc text-left">
                        <div class="clip">
                            <div class="bg bg-bg-chrome act bg-contain"
                                 style="background-image:url(/wp-content/themes/tropictour/img/home_1/country_map.png)">
                            </div>
                        </div>
                        <div class="vertical-align">
                            <?= forceTrans(get_field('about-text-1')) ?>
                            <div class="row btn-row about-btn-row">
                                <a href="#" class="c-button b-50 bg-aqua hv-transparent fr">
                                    <span>Подробнее</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-md-pull-6 col-sm-12">
                    <div class="row">
                        <?php foreach (array_slice(get_field('about-gallery-1'), 0, 4) as $imageId) : ?>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="radius-mask popular-img">
                                    <div class="clip">
                                        <div class="bg bg-bg-chrome act"
                                             style="background-image:url(<?= getImageUrl($imageId, 'medium') ?>)">
                                        </div>
                                    </div>
                                    <div class="tour-layer delay-1"></div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="row about-row">
                <div class="col-xs-12 col-md-6 col-sm-12">
                    <div class="popular-desc text-right">
                        <div class="clip">
                            <div class="bg bg-bg-chrome act bg-contain"
                                 style="background-image:url(/wp-content/themes/tropictour/img/home_1/country_map.png)">
                            </div>
                        </div>
                        <div class="vertical-align">
                            <?= forceTrans(get_field('about-text-2')) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-sm-12">
                    <div class="row">
                        <?php foreach (array_slice(get_field('about-gallery-2'), 0, 4) as $imageId) : ?>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="radius-mask popular-img">
                                    <div class="clip">
                                        <div class="bg bg-bg-chrome act"
                                             style="background-image:url(<?= getImageUrl($imageId, 'medium') ?>)">
                                        </div>
                                    </div>
                                    <div class="tour-layer delay-1"></div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php if ($reviews = get_field('reviews')) : ?>
    <div class="main-wraper padd-90 testimonials-section">
        <div class="tour-layer"></div>
        <img class="center-image" src="/wp-content/themes/tropictour/img/home_8/beach-bg.jpg" alt="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <h4 class="subtitle color-white underline">Отзывы</h4>
                        <h2 class="color-white">Нас ценят за наш подход</h2>
                    </div>
                </div>
            </div>
            <div class="arrows">
                <div class="swiper-container testi-slider" data-autoplay="0" data-loop="0" data-speed="900"
                     data-center="0" data-slides-per-view="1">
                    <div class="swiper-wrapper">
                        <?php foreach ($reviews as $review) : $post = get_post($review['id']) ?>
                            <div class="swiper-slide active" data-val="0">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                                            <div class="sg-testimonals">
                                                <div class="sg-image" style="background-image:url('/wp-content/themes/tropictour/img/home_8/person.jpg')">
                                                    <img class="sg-image"
                                                         src="<?= getPostThumbnailUrl('thumbnail') ?>" alt="<?php the_title() ?>">
                                                </div>
                                                <h3 class="color-white"><?php the_title() ?></h3>
                                                <p class="f-14 color-white-light"><?php the_content() ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; wp_reset_postdata() ?>
                    </div>
                    <div class="pagination pagination-hidden poin-style-1"></div>
                    <div class="arrow-wrapp arr-s-5">
                        <div class="cont-1170">
                            <div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
                            <div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($blocks = get_field('bottom-info')) : ?>
    <div class="main-wraper bg-grey-2 padd-90">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="second-title">
                        <!-- <h4 class="subtitle color-blue-2 underline">our services</h4> -->
                        <h2>3 шага путешественника</h2>
                    </div>
                </div>
            </div>
            <div class="row steps-row">
                <?php foreach ($blocks as $block) : ?>
                    <div class="col-xs-12 col-sm-4 steps-block">
                        <div class="icon-block style-2 bg-white">
                            <img class="icon-img bg-blue-2 border-grey-2"
                                 src="<?= getImageUrl($block['image']) ?>" alt="<?= $block['title'] ?>">
                            <h5 class="icon-title color-dark-2"><?= $block['title'] ?></h5>
                            <div class="icon-text color-dark-2-light"><?= $block['desctription'] ?></div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
<?php endif ?>

<?php get_footer() ?>