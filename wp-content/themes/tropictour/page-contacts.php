<?php get_header(); while (have_posts()) : the_post() ?>
<?php get_template_part('template-parts/layout/banner') ?>
<div class="detail-wrapper">
    <!-- CONTACT-FORM -->
    <div class="main-wraper padd-40">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <form class="contact-form" action="#">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-style-1 type-2 color-2">
                                    <input type="text" required="" placeholder="Ваше имя">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-style-1 type-2 color-2">
                                    <input type="text" required="" placeholder="Ваш e-mail">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="input-style-1 type-2 color-2">
                                    <input type="text" required="" placeholder="Ваше что то еще">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="drop-wrap drop-wrap-s-3 color-2">
                                    <div class="drop">
                                        <b>Выбор</b>
                                        <a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
                                        <span>
									    <a href="#">01 kids</a>
										<a href="#">02 kids</a>
										<a href="#">03 kids</a>
										<a href="#">04 kids</a>
										<a href="#">05 kids</a>
									</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <textarea class="area-style-1 color-1" required="" placeholder="Ваш комментарий"></textarea>
                                <button type="submit" class="c-button bg-aqua hv-dr-blue-2-o"><span>отправить</span></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="contact-about">
                        <h4 class="color-dark-2"><strong>Контактная информация</strong></h4>
                        <p class="color-grey-3"><?= forceTrans(get_field('description')) ?></p>
                    </div>
                    <div class="contact-info">
                        <div class="contact-line color-grey-3">
                            <img src="/wp-content/themes/tropictour/img/phone_icon_2_dark.png" alt="">
                            Телефон: <a class="color-dark-2" href="tel:<?= forceTrans(get_field('phone')) ?>"><?= forceTrans(get_field('phone')) ?></a>
                        </div>
                        <div class="contact-line color-grey-3">
                            <img src="/wp-content/themes/tropictour/img/mail_icon_b_dark.png" alt="">
                            E-mail: <a class="color-dark-2 tt" href="mailto:<?= forceTrans(get_field('email')) ?>">
                                <?= forceTrans(get_field('email')) ?>
                            </a>
                        </div>
                        <div class="contact-line color-grey-3">
                            <img src="/wp-content/themes/tropictour/img/loc_icon_dark.png" alt="">
                            Адрес: <span class="color-dark-2 tt"><?= forceTrans(get_field('address')) ?></span>
                        </div>
                    </div>
                    <div class="contact-socail">
                        <a class="color-grey-3 link-dr-blue-2" href="<?= forceTrans(get_field('facebook')) ?>" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a class="color-grey-3 link-dr-blue-2" href="<?= forceTrans(get_field('instagram')) ?>" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a class="color-grey-3 link-dr-blue-2" href="<?= forceTrans(get_field('youtube')) ?>" target="_blank">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php if ($map = get_field('map')) : ?>
    <div class="map-block">
        <div id="map-canvas" class="style-2" data-lat="<?= $map['lat'] ?>" data-lng="<?= $map['lng'] ?>" data-zoom="<?= $map['zoom'] ?>" data-style="2"></div>
        <div class="addresses-block">
            <a data-lat="<?= $map['lat'] ?>" data-lng="<?= $map['lng'] ?>" data-string="<?= $map['address'] ?>"></a>
        </div>
    </div>
<?php endif ?>
<script src="/wp-content/themes/tropictour/js/jquery-2.1.4.min.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;language=en"></script>
<script src="/wp-content/themes/tropictour/js/map.js"></script>
<?php endwhile; get_footer() ?>