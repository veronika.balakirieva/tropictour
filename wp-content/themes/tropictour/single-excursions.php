<?php get_header(); while (have_posts()) : the_post() ?>
    <?php get_template_part('template-parts/layout/banner') ?>
    <div class="detail-wrapper layout-two">
        <div class="container">
            <div class="row padd-90">
                <div class="col-xs-12 col-md-3">
                    <div class="right-sidebar">
                        <div class="sidebar-block">
                            <h4 class="sidebar-title color-dark-2">Категории</h4>
                            <ul class="sidebar-category color-3">
                                <?php foreach (getCategoriesArray('excursion-categories') as $i => $category) : ?>
                                    <li <?php if (! $i) echo 'class="active"' ?>>
                                        <a class="cat-drop" href="#">
                                            <?= $category['name'] ?>
                                            <span class="fr">(<?= $category['count'] ?>)</span>
                                        </a>
                                        <?php if (count($category['kids'])) : ?>
                                            <ul>
                                                <?php foreach ($category['kids'] as $kid) : ?>
                                                    <li>
                                                        <a href="<?= get_term_link($kid['id'], 'excursion-categories') ?>">
                                                            <?= $kid['name'] ?> (<?= $kid['count'] ?>)
                                                        </a>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="detail-header style-2">
                        <h2 class="detail-title color-dark-2"><?php the_title() ?></h2>
                        <div class="tour-info-line clearfix">
                            <div class="tour-info fl">
                                <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                <span class="font-style-2 color-dark-2"><?= get_the_date("d.m.Y") ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="detail-content">
                        <div class="detail-content-block">
                            <?php get_template_part('template-parts/layout/gallery') ?>
                            <?php the_content() ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="right-sidebar">

                        <?php get_template_part('template-parts/layout/help') ?>

                        <div class="form-wrap excursion-form-wrap">
                            <div class="form-row btn-row">
                                <a href="booking" class="c-button bg-aqua">
                                    <span><?= trans('Забронировать', 'Booking') ?></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; get_footer() ?>