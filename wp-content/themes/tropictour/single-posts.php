<?php get_header(); while (have_posts()) : the_post() ?>
<?php get_template_part('template-parts/layout/banner') ?>
<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                <div class="detail-header style-3">
                    <h2 class="detail-title color-dark-2"><?php the_title() ?></h2>
                    <div class="tour-info-line clearfix">
                        <div class="tour-info">
                            <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                            <span class="font-style-2 color-dark-2"><?= get_the_date("d.m.Y") ?></span>
                        </div>
                    </div>
                </div>
                <div class="detail-content">
                    <div class="detail-content-block">
                        <?php get_template_part('template-parts/layout/gallery') ?>
                        <?php the_content() ?>
                    </div>
                </div>
                <div class="blog-navigation">
                    <div class="row">
                        <?php if ($post = get_next_post()) : ?>
                            <div class="col-xs-6 col-md-6">
                                <div class="blog-nav-left">
                                    <a class="blog-nav-img black-hover" href="#">
                                        <img class="img-responsive" src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>">
                                        <div class="tour-layer delay-1"></div>
                                    </a>
                                    <div class="blog-nav-text">
                                        <a class="c-button b-26 bg-aqua" href="<?php the_permalink() ?>">
                                            <span><?= trans('Следующий', 'Next') ?></span>
                                        </a>
                                        <div class="tour-info-line">
                                            <div class="tour-info">
                                                <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                                <span class="font-style-2 color-dark-2"><?= get_the_date("d.m.Y") ?></span>
                                            </div>
                                        </div>
                                        <h4>
                                            <a class="color-dark-2 link-dr-blue-2" href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        <?php endif; wp_reset_postdata(); if ($post = get_previous_post()) : ?>
                            <div class="col-xs-6 col-md-6">
                                <div class="blog-nav-right">
                                    <a class="blog-nav-img black-hover" href="#">
                                        <img class="img-responsive" src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>">
                                        <div class="tour-layer delay-1"></div>
                                    </a>
                                    <div class="blog-nav-text">
                                        <a class="c-button b-26 bg-aqua" href="<?php the_permalink() ?>">
                                            <span><?= trans('Предидущий', 'Previous') ?></span>
                                        </a>
                                        <div class="tour-info-line">
                                            <div class="tour-info">
                                                <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png" alt="">
                                                <span class="font-style-2 color-dark-2"><?= get_the_date("d.m.Y") ?></span>
                                            </div>
                                        </div>
                                        <h4>
                                            <a class="color-dark-2 link-dr-blue-2" href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endwhile; get_footer() ?>