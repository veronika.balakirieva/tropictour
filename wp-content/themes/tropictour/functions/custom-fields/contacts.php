<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'contactPageCustomFields');
function contactPageCustomFields ()
{
    Container::make('post_meta', 'Контактная информация')
        ->where('post_id', '=', 22)
        ->add_fields([

            Field::make('textarea', 'description', 'Котороткая информация')
                ->set_required(true),

            Field::make('text', 'phone', 'Номер телефона')
                ->set_classes('do-not-translate')
                ->set_required(true),

            Field::make('text', 'email', 'Почта')
                ->set_classes('do-not-translate')
                ->set_attribute('type', 'email')
                ->set_required(true),

            Field::make('text', 'address', 'Адрес')
                ->set_required(true),

            Field::make('map', 'map', 'Точка на карте')
                ->set_classes('do-not-translate')
                ->set_position(7.951933, 98.338089, 6),

            Field::make('text', 'facebook', 'Facebook')
                ->set_classes('do-not-translate')
                ->set_required(true),

            Field::make('text', 'instagram', 'Instagram')
                ->set_classes('do-not-translate')
                ->set_required(true),

            Field::make('text', 'vk', 'Vkontakte')
                ->set_classes('do-not-translate')
                ->set_required(true),

            Field::make('text', 'youtube', 'Youtube')
                ->set_classes('do-not-translate')
                ->set_required(true),

        ]);
}
