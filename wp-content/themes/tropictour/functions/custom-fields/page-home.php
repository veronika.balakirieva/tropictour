<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'homePageCustomFields');
function homePageCustomFields ()
{
    Container::make('post_meta', 'Дополнительные поля')
        ->where('post_id', '=', 8)
        ->add_fields([

            /**
             * Slider
             */
            Field::make('complex', 'slider', 'Слайдер')
                ->set_min(1)
                ->set_layout('tabbed-horizontal')
                ->setup_labels([
                    'plural_name'   => 'слайды',
                    'singular_name' => 'слайд',
                ])
                ->add_fields([
                    Field::make('image', 'image', 'Изображение'),
                    Field::make('text', 'title', 'Заголовок'),
                    Field::make('text', 'desctription', 'Подпись'),
                    Field::make('text', 'url-title', 'Заголовок ссылки'),
                    Field::make('text', 'url-href', 'Ссылка')
                        ->set_classes('do-not-translate'),
                ]),

            /**
             * Excursions
             */
            Field::make('text', 'above-excursions', 'Надпись над экскурсиями'),
            Field::make('association', 'excursions', 'Экскурсии')
                ->set_types([
                    [ 'type' => 'term', 'taxonomy' => 'excursion-categories' ],
                ])
                ->set_min(3)
                ->set_max(6),

            /**
             * Top excursions
             */
            Field::make('association', 'top-excursions', 'Топ экскурсии')
                ->set_types([
                    [ 'type' => 'post', 'post_type' => 'excursions' ],
                ])
                ->set_min(3)
                ->set_max(6),

            /**
             * Popular excursions
             */
            Field::make('association', 'popular-excursions', 'Популярные экскурсии')
                ->set_types([
                    [ 'type' => 'post', 'post_type' => 'excursions' ],
                ])
                ->set_min(3)
                ->set_max(6),

            /**
             * Services
             */
            Field::make('text', 'above-services', 'Надпись над услугами'),
            Field::make('association', 'services', 'Услуги')
                ->set_types([
                    [ 'type' => 'post', 'post_type' => 'services' ],
                ])
                ->set_min(3)
                ->set_max(6),

            /**
             * Yachts
             */
            Field::make('text', 'above-yachts', 'Надпись над яхтами'),
            Field::make('association', 'yachts', 'Яхты')
                ->set_types([
                    [ 'type' => 'post', 'post_type' => 'yachts' ],
                ])
                ->set_min(2),

            /**
             * Posts
             */
            Field::make('association', 'posts', 'Актуальное по теме')
                ->set_types([
                    [ 'type' => 'post', 'post_type' => 'posts' ],
                ])
                ->set_min(3)
                ->set_max(3),

            /**
             * About company
             */
            Field::make('text', 'about-excerpt', 'Надпись над блоком "О компании"'),
            Field::make('rich_text', 'about-text-1', 'Тестовое поле 1'),
            Field::make('media_gallery', 'about-gallery-1', 'Галерея 1'),
            Field::make('rich_text', 'about-text-2', 'Тестовое поле 2'),
            Field::make('media_gallery', 'about-gallery-2', 'Галерея 2'),

            /**
             * Reviews
             */
            Field::make('association', 'reviews', 'Отзывы')
                ->set_types([
                    [ 'type' => 'post', 'post_type' => 'reviews' ],
                ]),

            /**
             * Info block
             */
            Field::make('complex', 'bottom-info', 'Информация внизу страницы')
                ->set_min(3)
                ->set_max(6)
                ->set_layout('tabbed-horizontal')
                ->setup_labels([
                    'plural_name'   => 'блоки',
                    'singular_name' => 'блок',
                ])
                ->add_fields([
                    Field::make('text', 'title', 'Заголовок'),
                    Field::make('text', 'desctription', 'Подпись'),
                    Field::make('image', 'image', 'Изображение'),
                ]),

        ]);
}
