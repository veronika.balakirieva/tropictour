<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Add programs with prices to excursions
 */
add_action('carbon_fields_register_fields', 'addProgramsToExcursions');
function addProgramsToExcursions ()
{
    Container::make('post_meta', 'Дополнительные опции')
        ->where('post_type', '=', 'excursions')
        ->add_fields([

            Field::make('complex', 'programs', 'Программы')
                ->set_min(1)
                ->set_layout('tabbed-horizontal')
                ->setup_labels([
                    'plural_name'   => 'программы',
                    'singular_name' => 'программу',
                ])
                ->add_fields([
                    Field::make('text', 'title', 'Название'),
                    Field::make('rich_text', 'desctription', 'Описание'),
                    Field::make('text', 'price-1', 'Цена 1')
                        ->set_attribute('type', 'number')
                        ->set_required(true)
                        ->set_classes('do-not-translate'),

                    Field::make('text', 'price-1-sale', 'Цена 1 со скидкой')
                        ->set_attribute('type', 'number')
                        ->set_classes('do-not-translate'),

                    Field::make('text', 'price-2', 'Цена 2')
                        ->set_attribute('type', 'number')
                        ->set_required(true)
                        ->set_classes('do-not-translate'),

                    Field::make('text', 'price-2-sale', 'Цена 2 со скидкой')
                        ->set_attribute('type', 'number')
                        ->set_classes('do-not-translate'),
                ]),

        ]);
}

/**
 * Add prices for districts transfer
 */
add_action('carbon_fields_register_fields', 'addDistrictPricesToExcursions');
function addDistrictPricesToExcursions ()
{
    $districtsQuery = getDistrictsQuery();
    if ($districtsQuery->have_posts()) {
        $fields = [];
        while ($districtsQuery->have_posts()) {
            $districtsQuery->the_post();
            $fields[] = Field::make('text', 'district-price-' . get_the_ID(), get_the_title())
                ->set_attribute('type', 'number')
                ->set_required(true)
                ->set_classes('do-not-translate');
        }
        wp_reset_postdata();
        Container::make('post_meta', 'Стоимость трансфера с районов')
            ->where('post_type', '=', 'excursions')
            ->add_fields($fields);
    }
}