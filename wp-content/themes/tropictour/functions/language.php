<?php

if (! function_exists('trans')) {
    /**
     * Get translated strings
     *
     * @param string $ru
     * @param string $en
     * @return string
     */
    function trans (string $ru, string $en) : string
    {
        return qtranxf_getLanguage() == "ru" ? $ru : $en;
    }
}

if (! function_exists('forceTrans')) {
    /**
     * Force trans string using qTranslate
     *
     * @param string $string
     * @return string
     */
    function forceTrans ($string) : string
    {
        if (! $string) return '';

        return qtranxf_use(qtranxf_getLanguage(), $string, false);
    }
}

if (! function_exists('isRussian')) {
    /**
     * Get is Russian locale statement
     *
     * @return bool
     */
    function isRussian () : bool
    {
        return qtranxf_getLanguage() === "ru";
    }
}

if (! function_exists('getLanguageLink')) {
    /**
     * Get array for language switcher
     *
     * @return string
     */
    function getLanguageLink() : string
    {
        return isRussian() ? '/en/' : '/ru/';
    }
}