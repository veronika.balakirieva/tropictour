<?php

/**
 * Register excurion's categories taxonomy
 */
if (! function_exists('registerExcurisonCategories')) {
    function registerExcurisonCategories ()
    {
        register_taxonomy("excursion-categories", [ 'excursions' ], [
            "labels"       => [
                "name"                  => "Категории экскурсий",
                "singular_name"         => "Категория",
                "menu_name"             => "Категории",
                "all_items"             => "Другие категории",
                "new_item_name"         => "Новая категория",
                "add_new_item"          => "Добавить новую категорию",
                "edit_item"             => "Изменить категорию",
                "update_item"           => "Обновить категорию",
                "search_items"          => "Найти",
                "add_or_remove_items"   => "Найти или удалить",
                "choose_from_most_used" => "Поиск среди популярных",
                "not_found"             => "Не найдено",
            ],
            "hierarchical" => true,
            "public"       => true,
        ]);
    }

    add_action("init", "registerExcurisonCategories");
}
/**
 * Register yacht's categories taxonomy
 */
if (! function_exists('registerYachtCategories')) {
    function registerYachtCategories ()
    {
        register_taxonomy("yachts-categories", [ 'yachts' ], [
            "labels"       => [
                "name"                  => "Категории яхт",
                "singular_name"         => "Категория",
                "menu_name"             => "Категории",
                "all_items"             => "Другие категории",
                "new_item_name"         => "Новая категория",
                "add_new_item"          => "Добавить новую категорию",
                "edit_item"             => "Изменить категорию",
                "update_item"           => "Обновить категорию",
                "search_items"          => "Найти",
                "add_or_remove_items"   => "Найти или удалить",
                "choose_from_most_used" => "Поиск среди популярных",
                "not_found"             => "Не найдено",
            ],
            "hierarchical" => true,
            "public"       => true,
        ]);
    }

    add_action("init", "registerYachtCategories");
}

///**
// * Register service's categories taxonomy
// */
//if (! function_exists('registerServiceCategories')) {
//    function registerServiceCategories ()
//    {
//        register_taxonomy("services-categories", [ 'services' ], [
//            "labels"       => [
//                "name"                  => "Категории услуг",
//                "singular_name"         => "Категория",
//                "menu_name"             => "Категории",
//                "all_items"             => "Другие категории",
//                "new_item_name"         => "Новая категория",
//                "add_new_item"          => "Добавить новую категорию",
//                "edit_item"             => "Изменить категорию",
//                "update_item"           => "Обновить категорию",
//                "search_items"          => "Найти",
//                "add_or_remove_items"   => "Найти или удалить",
//                "choose_from_most_used" => "Поиск среди популярных",
//                "not_found"             => "Не найдено",
//            ],
//            "hierarchical" => true,
//            "public"       => true,
//        ]);
//    }
//
//    add_action("init", "registerServiceCategories");
//
//}

if (! function_exists('registerPostCategories')) {
    function registerPostCategories ()
    {
        register_taxonomy("posts-categories", [ 'posts' ], [
            "hierarchical" => true,
            "public"       => true,
        ]);
    }

    add_action("init", "registerPostCategories");
}

if (! function_exists('registerPostTags')) {
    function registerPostTags ()
    {
        register_taxonomy("posts-tags", [ 'posts' ], [
            "hierarchical" => false,
            "public"       => true,
        ]);
    }

    add_action("init", "registerPostTags");
}
