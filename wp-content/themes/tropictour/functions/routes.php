<?php

/**
 * Register custom query variables
 */
add_filter('query_vars', 'addCustomQueryVars');
function addCustomQueryVars ($query_vars)
{
    $query_vars[] = 'booking';
    return $query_vars;
}

/**
 * Custom route for excursion booking
 */
add_action('init', 'bookingRoute');
function bookingRoute ()
{
    add_rewrite_rule(
        '^excursions/([^/]+)/booking/?$',
        'index.php?post_type=excursions&name=$matches[1]&booking=1',
        'top');
}

/**
 * Custom template loader
 */
add_action('template_redirect', 'redirector');
function redirector() {

    /**
     * Redirect on booking template
     */
    if (is_singular('excursions') && get_query_var('booking')) {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            dd('TODO PAYPAL PAY');
        } else {
            add_filter('template_include', function() {
                return get_template_directory() . '/single-excursions-booking.php';
            });
        }
    }

}