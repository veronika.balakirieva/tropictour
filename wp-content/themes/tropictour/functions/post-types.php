<?php

/**
 * Register excursions post type
 */
if (! function_exists('registerExcursions')) {
    function registerExcursions ()
    {
        register_post_type("excursions", [
            'labels'        => [
                'name'               => "Экскурсии",
                'singular_name'      => "Экскурсия",
                'add_new'            => "Добавить экскурсию",
                'add_new_item'       => "Добавить экскурсию",
                'edit_item'          => "Изменить экскурсию",
                'new_item'           => "Новая экскурсия",
                'view_item'          => "Все экскурсии",
                'search_items'       => "Результаты поиска",
                'not_found'          => "Ничего не найдено",
                'not_found_in_trash' => "Ничего не найдено",
                'menu_name'          => "Экскурсии",
            ],
            'menu_position' => 5,
            'menu_icon'     => "dashicons-palmtree",
            'public'        => true,
            'has_archive'   => true,
            'supports'      => [ 'title', 'editor', 'thumbnail' ],
        ]);
    }

    add_action("init", "registerExcursions");
}

/**
 * Register yachts post type
 */
if (! function_exists('registerYachts')) {
    function registerYachts ()
    {
        register_post_type("yachts", [
            'labels'        => [
                'name'               => "Яхты",
                'singular_name'      => "Яхты",
                'add_new'            => "Добавить яхту",
                'add_new_item'       => "Добавить яхту",
                'edit_item'          => "Изменить яхту",
                'new_item'           => "Новая яхта",
                'view_item'          => "Все яхты",
                'search_items'       => "Результаты поиска",
                'not_found'          => "Ничего не найдено",
                'not_found_in_trash' => "Ничего не найдено",
                'menu_name'          => "Яхты",
            ],
            'menu_position' => 6,
            'menu_icon'     => "dashicons-sos",
            'public'        => true,
            'has_archive'   => true,
            'supports'      => [ 'title', 'editor', 'thumbnail' ],
        ]);
    }

    add_action("init", "registerYachts");
}

/**
 * Register services post type
 */
if (! function_exists('registerServices')) {
    function registerServices ()
    {
        register_post_type("services", [
            'labels'        => [
                'name'               => "Услуги",
                'singular_name'      => "Услуга",
                'add_new'            => "Добавить услугу",
                'add_new_item'       => "Добавить услугу",
                'edit_item'          => "Изменить услугу",
                'new_item'           => "Новая услуга",
                'view_item'          => "Все услуги",
                'search_items'       => "Результаты поиска",
                'not_found'          => "Ничего не найдено",
                'not_found_in_trash' => "Ничего не найдено",
                'menu_name'          => "Услуги",
            ],
            'menu_position' => 7,
            'menu_icon'     => "dashicons-tickets-alt",
            'public'        => true,
            'has_archive'   => false,
            'supports'      => [ 'title', 'editor', 'thumbnail' ],
        ]);
    }

    add_action("init", "registerServices");
}

/**
 * Register reviews post type
 */
if (! function_exists('registerReviews')) {
    function registerReviews ()
    {
        register_post_type("reviews", [
            'labels'        => [
                'name'               => "Отзывы",
                'singular_name'      => "Отзыв",
                'add_new'            => "Добавить отзыв",
                'add_new_item'       => "Добавить отзыв",
                'edit_item'          => "Изменить отзыв",
                'new_item'           => "Новый отзыв",
                'view_item'          => "Все отзывы",
                'search_items'       => "Результаты поиска",
                'not_found'          => "Ничего не найдено",
                'not_found_in_trash' => "Ничего не найдено",
                'menu_name'          => "Отзывы",
            ],
            'menu_position' => 8,
            'menu_icon'     => "dashicons-format-status",
            'public'        => true,
            'has_archive'   => true,
            'supports'      => [ 'title', 'editor', 'thumbnail', ],
        ]);
    }

    add_action("init", "registerReviews");
}

/**
 * Register districts
 */
if (! function_exists('registerDistricts')) {
    function registerDistricts ()
    {
        register_post_type("districts", [
            'labels'        => [
                'name'               => "Районы",
                'singular_name'      => "Район",
                'add_new'            => "Добавить район",
                'add_new_item'       => "Добавить район",
                'edit_item'          => "Изменить район",
                'new_item'           => "Новый район",
                'view_item'          => "Все районы",
                'search_items'       => "Результаты поиска",
                'not_found'          => "Ничего не найдено",
                'not_found_in_trash' => "Ничего не найдено",
                'menu_name'          => "Районы",
            ],
            'menu_position' => 9,
            'public'        => true,
            'has_archive'   => false,
            'supports'      => [ 'title', ],
        ]);
    }

    add_action("init", "registerDistricts");
}

/**
 * Remove old posts and register new posts post type below
 */

if (! function_exists('remove_default_post_type')) {
    function remove_default_post_type ()
    {
        remove_menu_page('edit.php');
    }

    add_action('admin_menu', 'remove_default_post_type');
}

if (! function_exists('registerNewPosts')) {
    function remove_default_post_type_menu_bar ($wp_admin_bar)
    {
        $wp_admin_bar->remove_node('new-post');
    }

    add_action('admin_bar_menu', 'remove_default_post_type_menu_bar', 999);
}

if (! function_exists('registerNewPosts')) {
    function remove_draft_widget ()
    {
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    }

    add_action('wp_dashboard_setup', 'remove_draft_widget', 999);
}

if (! function_exists('registerNewPosts')) {

    function registerNewPosts ()
    {
        register_post_type("posts", [
            'menu_position' => 4,
            'public'        => true,
            'has_archive'   => true,
            'supports'      => [ 'title', 'editor', 'thumbnail' ],
        ]);
    }

    add_action("init", "registerNewPosts");
}