<?php

if (! function_exists('get_field')) {
    /**
     * Carbon fields helper on frontend
     *
     * @param string $fieldName
     * @param null $id
     * @return mixed
     */
    function get_field (string $fieldName, $id = null)
    {
        return $id
            ? carbon_get_post_meta($id, $fieldName)
            : carbon_get_the_post_meta($fieldName) ;
    }
}

if (! function_exists('the_field')) {
    /**
     * One more carbon fields helper on frontend
     *
     * @param string $fieldName
     */
    function the_field (string $fieldName, $id = null)
    {
        echo get_field($fieldName, $id);
    }
}


if (! function_exists('getMenuName')) {
    /**
     * Get valid menu name (helper for getMenuArray function)
     *
     * @param string $location
     * @return string
     */
    function getMenuName (string $location): string
    {
        $theme_locations = get_nav_menu_locations();
        $menu_obj = get_term($theme_locations[ $location ], 'nav_menu');
        return $menu_obj->name;
    }
}

if (! function_exists('getMenuArray')) {
    /**
     * Get menu in array type
     *
     * @param string $name
     * @return array
     */
    function getMenuArray (string $name): array
    {
        $array_menu = wp_get_nav_menu_items(getMenuName($name));
        $header_menu = [];
        foreach ($array_menu as $m) {
            if (empty($m->menu_item_parent)) {
                $header_menu[ $m->ID ] = [];
                $header_menu[ $m->ID ][ 'ID' ] = $m->ID;
                $header_menu[ $m->ID ][ 'title' ] = $m->title;
                $header_menu[ $m->ID ][ 'url' ] = $m->url;
                $header_menu[ $m->ID ][ 'children' ] = [];
            }
        }

        $submenu = [];
        foreach ($array_menu as $m) {
            if ($m->menu_item_parent) {
                $submenu[ $m->ID ] = [];
                $submenu[ $m->ID ][ 'ID' ] = $m->ID;
                $submenu[ $m->ID ][ 'title' ] = $m->title;
                $submenu[ $m->ID ][ 'url' ] = $m->url;
                $header_menu[ $m->menu_item_parent ][ 'children' ][ $m->ID ] = $submenu[ $m->ID ];
            }
        }

        return $header_menu;
    }
}

if (! function_exists('getPostThumbnailUrl')) {
    /**
     * Get post thumbnail url
     *
     * @param string $size
     * @param string $post_id
     * @return string
     */
    function getPostThumbnailUrl (string $size = "large", $post_id = "")
    {
        global $post;
        if (! $post_id) $post_id = $post->ID;
        $thumb_id = get_post_thumbnail_id($post_id);
        return getImageUrl($thumb_id, $size);
    }
}

if (! function_exists('getImageUrl')) {
    /**
     * Get image url from id
     *
     * @param $id
     * @param string $size
     * @return string
     */
    function getImageUrl ($id, string $size = "large")
    {
        return ($id)
            ? wp_get_attachment_image_src($id, $size, true)[ 0 ]
            : "/wp-content/themes/tropictour/img/default.png";
    }
}

if (! function_exists('getCategoriesArray')) {
    /**
     * Get hierarchial array of categories
     *
     * @param string $taxonomy
     * @param bool $withPosts
     * @return array
     */
    function getCategoriesArray (string $taxonomy, bool $withPosts = false): array
    {
        $terms = get_terms([
            'taxonomy'   => $taxonomy,
            'hide_empty' => false,
            'parent'     => null,
        ]);

        $result = [];

        foreach ($terms as $term)
            $result[] = [
                'id'    => $term->term_id,
                'name'  => $term->name,
                'slug'  => $term->slug,
                'count' => $term->count,
                'kids'  => array_map(function ($termKid) {
                    return [
                        'id'    => $termKid->term_id,
                        'name'  => $termKid->name,
                        'slug'  => $termKid->slug,
                        'count' => $termKid->count,
                    ];
                }, get_terms([
                    'taxonomy'   => $taxonomy,
                    'hide_empty' => false,
                    'parent'     => $term->term_id,
                ])),
            ];

        return $result;
    }
}

if (! function_exists('getBannerImage')) {
    /**
     * Get banner image almost for all pages
     *
     * @return string
     */
    function getBannerImage ()
    {
        $imageId = get_field('banner-image') ?: carbon_get_theme_option('default-banner-image');
        return getImageUrl($imageId, 'full');
    }
}

if (! function_exists('getRelatedYachts')) {
    /**
     * Get related yachts
     *
     * @param $yacht
     * @return WP_Query
     */
    function getRelatedYachts ($yacht)
    {
        $taxIds = array_map(function ($cat) {
            return $cat->term_id;
        }, wp_get_post_terms($yacht->ID, 'yachts-categories'));
        return new WP_Query([
            'post_type' => 'yachts',
            'post__not_in' => [ $yacht->ID ],
            'tax_query'    => [
                'relation' => 'AND',
                [
                    'taxonomy' => 'yachts-categories',
                    'field'    => 'term_id',
                    'terms'    => $taxIds,
                ],
            ],
        ]);
    }
}

if (! function_exists('getDistrictsQuery')) {
    /**
     * Get districts query
     *
     * @return WP_Query
     */
    function getDistrictsQuery()
    {
        return new WP_Query([ 'post_type' => 'districts' ]);
    }
}