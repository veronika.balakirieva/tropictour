<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('after_setup_theme', 'crb_load');
function crb_load ()
{
    require_once(get_template_directory() . '/vendor/autoload.php');
    \Carbon_Fields\Carbon_Fields::boot();
}

add_action('carbon_fields_register_fields', 'crb_attach_theme_options');
function crb_attach_theme_options ()
{
    Container::make('theme_options', 'Прочее')
        ->add_fields([
            Field::make('image', 'default-banner-image', 'Изображение в баннере по умолчанию')
                ->set_required(true),
            Field::make('textarea', 'need-help-text', 'Описание в блоке "Нужна помощь?"')
                ->set_required(true),
        ]);
}

add_action('carbon_fields_register_fields', 'addGalleryToAllPostTypes');
function addGalleryToAllPostTypes ()
{
    Container::make('post_meta', 'Галерея')
        ->where('post_type', 'IN', [ 'excursions', 'yachts', 'services', 'posts', ])
        ->add_fields([
            Field::make('media_gallery', 'gallery', 'Выберите изображения'),
        ]);
}

add_action('carbon_fields_register_fields', 'addExcerptToAllPostTypes');
function addExcerptToAllPostTypes ()
{
    Container::make('post_meta', 'Короткое описание')
        ->where('post_type', 'IN', [ 'excursions', 'yachts', 'services', 'posts', ])
        ->add_fields([
            Field::make('textarea', 'excerpt', 'Введите короткое описание')
                ->set_required(true),
        ]);
}

add_action('carbon_fields_register_fields', 'addImageToCategories');
function addImageToCategories ()
{
    Container::make('term_meta', 'Дополнительно')
        ->where('term_taxonomy', 'IN', [ 'excursion-categories', 'yachts-categories', ])
        ->add_fields([
            Field::make('image', 'image', 'Изображение'),
        ]);
}

add_action('carbon_fields_register_fields', 'bannerSettings');
function bannerSettings ()
{
    Container::make('post_meta', 'Баннер')
        ->where('post_type', 'IN', [ 'excursions', 'yachts', 'services', 'posts', ])
        ->add_fields([
            Field::make('image', 'banner-image', 'Выберите изображения'),
        ]);
}

require 'custom-fields/page-home.php';
require 'custom-fields/excursions.php';
require 'custom-fields/contacts.php';