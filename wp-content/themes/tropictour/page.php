<?php get_header(); while (have_posts()) : the_post() ?>
<?php get_template_part('template-parts/layout/banner') ?>
<div class="detail-wrapper">
    <div class="main-wraper padd-40">
        <div class="container"><?php the_content() ?></div>
    </div>
</div>
<?php endwhile; get_footer() ?>