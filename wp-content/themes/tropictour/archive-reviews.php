<?php get_header() ?>
<?php get_template_part('template-parts/layout/banner') ?>
    <div class="detail-wrapper">
        <div class="container">
            <div class="row padd-90">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="additional-block padd-90">
                        <ul class="comments-block">
                            <?php while (have_posts()) : the_post() ?>
                                <li class="comment-entry clearfix">
                                    <div class="comment-img" style="background-image: url('<?= getPostThumbnailUrl() ?>')"></div>
                                    <!-- <img class="commnent-img"
                                         src="<?= getPostThumbnailUrl() ?>" alt="<?php the_title() ?>"> -->
                                    <div class="comment-content clearfix">
                                        <div class="tour-info-line">
                                            <div class="tour-info">
                                                <img src="/wp-content/themes/tropictour/img/calendar_icon_grey.png"
                                                     alt="">
                                                <span class="font-style-2 color-dark-2"><?= get_the_date('d.m.Y') ?></span>
                                            </div>
                                            <div class="tour-info">
                                                <img src="/wp-content/themes/tropictour/img/people_icon_grey.png"
                                                     alt="">
                                                <span class="font-style-2 color-dark-2"><?php the_title() ?></span>
                                            </div>
                                        </div>
                                        <div class="comment-text color-grey"><?php the_content() ?></div>
                                    </div>
                                </li>
                            <?php endwhile ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>