<?php if ($program = get_field('programs')) : $program = $program[0] ?>
<div class="price">
    <div class="price-block">
        <span class="fa fa-male"></span>
        <h5>
            <span class=""><strong><?= $program['price-1'] ?></strong></span>
            <?php if ($price1Sale = $program['price-1-sale']) : ?>
                <span class="color-dark-2-light"><strong><s><?= $price1Sale ?></s></strong></span>
            <?php endif ?>
        </h5>
    </div>
    ,
    <div class="price-block">
        <span class="fa fa-child"></span>
        <h5>
            <span class="color-red"><strong><?= $program['price-2'] ?></strong></span>
            <?php if ($price2Sale = $program['price-2-sale']) : ?>
                <span class="color-dark-2-light"><strong><s><?= $price2Sale ?></s></strong></span>
            <?php endif ?>
        </h5>
    </div>
</div>
<?php endif ?>