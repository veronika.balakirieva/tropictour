<div class="inner-banner style-6">
    <img class="center-image" src="<?= getBannerImage() ?>" alt="">
    <div class="vertical-align">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <?php get_template_part('template-parts/layout/breadcrumbs') ?>
                </div>
            </div>
        </div>
    </div>
</div>