<?php if ($galleryIds = get_field('gallery')) : ?>
    <div class="slider-wth-thumbs style-2 arrows">
        <div class="swiper-container thumbnails-preview" data-autoplay="0" data-loop="1"
             data-speed="500" data-center="0" data-slides-per-view="1">
            <div class="swiper-wrapper">
                <?php foreach ($galleryIds as $i => $galleryId) : ?>
                    <div class="swiper-slide <?php if (! $i) echo 'active' ?>" data-val="0">
                        <img class="img-responsive img-full"
                             src="<?= getImageUrl($galleryId, 'full') ?>" alt="gallery image">
                    </div>
                <?php endforeach ?>
            </div>
            <div class="pagination pagination-hidden"></div>
            <div class="arrow-wrapp arr-s-3">
                <div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span>
                </div>
                <div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span>
                </div>
            </div>
        </div>

        <div class="swiper-container thumbnails" data-autoplay="0"
             data-loop="0" data-speed="500" data-center="0"
             data-slides-per-view="responsive" data-mob-slides="2" data-xs-slides="3"
             data-sm-slides="4" data-md-slides="5" data-lg-slides="5"
             data-add-slides="5">
            <div class="swiper-wrapper">
                <?php foreach ($galleryIds as $i => $galleryId) : ?>
                    <div class="swiper-slide current <?php if (! $i) echo 'active' ?>" data-val="0">
                        <img class="img-responsive img-full"
                             src="<?= getImageUrl($galleryId, 'thumbnail') ?>" alt="">
                    </div>
                <?php endforeach ?>
            </div>
            <div class="pagination hidden"></div>
        </div>
    </div>
<?php endif ?>