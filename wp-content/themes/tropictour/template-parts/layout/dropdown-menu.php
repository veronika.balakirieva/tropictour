<nav class="menu">
    <ul>
        <?php foreach (getMenuArray("header") as $item) : ?>
            <li class="type-1">
                <a href="<?= $item[ "url" ] ?>">
                    <?= $item[ "title" ] ?>
                    <span class="fa fa-angle-down"></span>
                </a>
                <?php if ($item[ "children" ]) : ?>
                    <ul class="dropmenu">
                        <?php foreach ($item[ "children" ] as $kid) : ?>
                            <li><a href="<?= $kid[ "url" ] ?>"><?= $kid[ "title" ] ?></a></li>
                        <?php endforeach ?>
                    </ul>
                <?php endif ?>
            </li>
        <?php endforeach ?>
    </ul>
</nav>