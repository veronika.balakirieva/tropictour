<meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta name="format-detection" content="telephone=no"/>

<link rel="shortcut icon" href="favicon.ico"/>
<link href="/wp-content/themes/tropictour/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="/wp-content/themes/tropictour/css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css"/>
<link href="/wp-content/themes/tropictour/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700" rel="stylesheet">
<link href="/wp-content/themes/tropictour/css/style.css" rel="stylesheet" type="text/css"/>
<link href="/wp-content/themes/tropictour/css/nikita.css" rel="stylesheet" type="text/css"/>
<link href="/wp-content/themes/tropictour/css/olga.css" rel="stylesheet" type="text/css"/>
<title><?php wp_title() ?></title>
<?php wp_head() ?>