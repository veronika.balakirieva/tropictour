<ul class="banner-breadcrumb color-white clearfix">
    <li><a class="link-blue-2" href="/">Главная</a> /</li>
    <?php if (is_single() || is_page()) : ?>
        <?php $pt = get_queried_object()->post_type; if ($pt == "posts") : ?>
            <li><a class="link-blue-2" href="/posts/">Блог</a></li>
        <?php elseif ($pt == "excursions") : ?>
            <?php $cats = get_the_terms(get_the_ID(), "excursion-categories"); if ($cats[1]) : ?>
                <li><a class="link-blue-2" href="/excursion-categories/<?= $cats[1]->slug ?>"><?= $cats[1]->name ?></a></li>
                <li><a class="link-blue-2" href="/excursion-categories/<?= $cats[1]->slug ?>/<?= $cats[0]->slug ?>/"><?= $cats[0]->name ?></a></li>
            <?php elseif ($cats[0]) : ?>
                <li><a class="link-blue-2" href="/excursion-categories/<?= $cats[0]->slug ?>/"><?= $cats[0]->name ?></a></li>
            <?php endif ?>
        <?php elseif ($pt == "yachts") : ?>
            <?php $cats = get_the_terms(get_the_ID(), "yachts-categories"); if ($cats[1]) : ?>
                <li><a class="link-blue-2" href="/yachts-categories/<?= $cats[1]->slug ?>"><?= $cats[1]->name ?></a></li>
                <li><a class="link-blue-2" href="/yachts-categories/<?= $cats[1]->slug ?>/<?= $cats[0]->slug ?>/"><?= $cats[0]->name ?></a></li>
            <?php elseif ($cats[0]) : ?>
                <li><a class="link-blue-2" href="/category/<?= $cats[0]->slug ?>/"><?= $cats[0]->name ?></a></li>
            <?php endif ?>
        <?php endif ?>
        <?php $title = get_queried_object()->post_title ?>
    <?php else : $cat = get_queried_object(); ?>
        <?php if ($cat) : ?>
            <?php if ($cat->taxonomy == "excursion-categories") : ?>
                <li><a class="link-blue-2" href="/excursion-categories/">Категории</a></li>
            <?php elseif ($cat->taxonomy == "yachts-categories") : ?>
                <?php if ($cat->parent) : $parent = get_term($cat->parent, "yachts-categories") ?>
                    <li><a class="link-blue-2" href="/yachts-categories/<?= $parent->slug ?>/"><?= $parent->name ?></a></li>
                <?php endif ?>
            <?php elseif ($cat->taxonomy == "manufacturers") : ?>
                <li><a class="link-blue-2" href="/manufacturers/">Производители</a></li>
            <?php endif ?>
            <?php $title = $cat->name ?>
        <?php else : ?>
            <li><a class="link-blue-2" href="/blog/">Блог</a></li>
        <?php endif ?>
    <?php endif ?>
</ul>
<h2 class="color-white"><?= forceTrans($title) ?></h2>