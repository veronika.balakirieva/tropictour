<div class="help-contact bg-grey-2">
    <h4 class="color-dark-2"><?= trans('Нужна помощь?', 'Need Help?') ?></h4>
    <p class="color-grey-2"><?= carbon_get_theme_option('need-help-text') ?></p>
    <a class="help-phone color-dark-2 link-dr-blue-2" href="tel:<?= get_field('phone', CONTACT_PAGE_ID) ?>">
        <img src="/wp-content/themes/tropictour/img/detail/phone24-dark-2.png" alt="">
        <?= get_field('phone', CONTACT_PAGE_ID) ?>
    </a>
    <a class="help-mail color-dark-2 link-dr-blue-2" href="mailto:<?= get_field('email', CONTACT_PAGE_ID) ?>">
        <img src="/wp-content/themes/tropictour/img/detail/letter-dark-2.png" alt="">
        <?= get_field('email', CONTACT_PAGE_ID) ?>
    </a>
</div>