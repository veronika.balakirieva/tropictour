<div class="wrap-langs">
    <input type="checkbox" id="lang-check" name="lang-check" class="lang-check">
    <label for="lang-check">
        <div class="active-lang"><?= isRussian() ? 'Рус' : 'En' ?></div>
    </label>
    <ul>
        <li><a href="<?= getLanguageLink() ?>"><?= isRussian() ? 'En' : 'Рус' ?></a></li>
    </ul>
    <div class="arrow-down lang-arrow-btn"></div>
</div>