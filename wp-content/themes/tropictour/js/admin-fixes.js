jQuery(document).ready(function() {

    let needle = '.carbon-textarea:not(.do-not-translate) textarea, .carbon-text:not(.do-not-translate) .regular-text';
    let divs = document.querySelectorAll(needle);
    for (let i = 0; i < divs.length; i++) {
        divs[i].classList.add('multilang');
    }

});