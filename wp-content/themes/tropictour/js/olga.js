

function calcMaxHeight(arr) {
	var maxHeight = 0;
	for (var i = 0; i < arr.length; i++) {
		arr[i].style.height = 'auto';
		// maxHeight = arr[i].offsetHeight;
		console.log(arr[i].offsetHeight + '>' + maxHeight);
		if(arr[i].offsetHeight > maxHeight) {
			maxHeight = arr[i].offsetHeight;
      console.log(maxHeight);
		}

	}
	// arr.forEach(function(item) {
	//  	item.style.height = 'auto';
	// 	// maxHeight = item.offsetHeight;
	// 	if(item.offsetHeight > maxHeight) {
	// 		maxHeight = item.offsetHeight;
	// 	}
	// })
	return maxHeight;
}

function setEqualHeight(arr) {

	var height = calcMaxHeight(arr);
	for (var i = 0; i < arr.length; i++) {
		arr[i].style.height = height + 'px';
	}
	// arr.forEach(function(item) {
	// 	item.style.height = height + 'px';
	// })
}

var stepsBlocks = document.querySelectorAll('.steps-block .icon-block');
var topExcursions = document.querySelectorAll('.top-exursion-item');
var exursionsDesc = document.querySelectorAll('.tour-desc');
var yachtsItems = document.querySelectorAll('.yacht-desc');

setTimeout(function() {
  setEqualHeight(yachtsItems);
}, 100)

setEqualHeight(stepsBlocks);
setEqualHeight(topExcursions);
setEqualHeight(exursionsDesc);


window.onresize = function() {
	setEqualHeight(stepsBlocks);
	setEqualHeight(topExcursions);
	setEqualHeight(exursionsDesc);
	setEqualHeight(yachtsItems);
}

