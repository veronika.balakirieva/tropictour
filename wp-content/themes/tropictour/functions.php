<?php

require 'functions/language.php';
require 'functions/routes.php';
require 'functions/custom-fields.php';
require 'functions/post-types.php';
require 'functions/taxonomies.php';
require 'functions/layout-helpers.php';

const HOME_PAGE_ID = 8;
const CONTACT_PAGE_ID = 22;

/**
 * Dump the passed variables and end the script.
 *
 * @param  mixed $args
 */
if (! function_exists('dd')) {
    function dd (...$args)
    {
        http_response_code(500);
        foreach ($args as $x) {
            var_dump($x);
            echo "<br><br>";
        }
        die(1);
    }
}

/**
 * Remove admin bar
 */
add_filter('show_admin_bar', '__return_false');

/**
 * Theme common settings
 */
if (! function_exists('themeCommonSettings')) {
    function themeCommonSettings ()
    {
        /**
         * Add menu support
         */
        register_nav_menus([
            'header' => 'В хедере',
            'footer' => 'В футере',
        ]);

        /**
         * Make theme supports thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 9999);

    }

    add_action('init', 'themeCommonSettings');
}

/**
 * Admin common settings
 */
const PAGES_WITHOUT_EDITOR = [ HOME_PAGE_ID, CONTACT_PAGE_ID ];
if (! function_exists('adminCommonSettings')) {
    function adminCommonSettings ()
    {

        /**
         * Include js to fix admin translatable fields
         */
         wp_enqueue_script('admin-fixes', get_template_directory_uri() . '/js/admin-fixes.js');

        /**
         * Remove the content editor from some pages pages
         */
        if ($_GET['post'] && in_array($_GET['post'], PAGES_WITHOUT_EDITOR)) {
            remove_post_type_support('page', 'editor');
        }


    }

    add_action('admin_init', 'adminCommonSettings');
}

/**
 * Remove all updates check
 */
if (is_admin()) {
    remove_action('admin_init', '_maybe_update_core');
    remove_action('admin_init', '_maybe_update_plugins');
    remove_action('admin_init', '_maybe_update_themes');
    remove_action('load-plugins.php', 'wp_update_plugins');
    remove_action('load-themes.php', 'wp_update_themes');
    remove_action('load-update-core.php', 'wp_update_plugins');
    remove_action('load-update-core.php', 'wp_update_themes');
    remove_action('load-update.php', 'wp_update_plugins');
    remove_action('load-update.php', 'wp_update_themes');
    remove_action('wp_version_check', 'wp_version_check');
    remove_action('wp_update_plugins', 'wp_update_plugins');
    remove_action('wp_update_themes', 'wp_update_themes');
    add_filter('pre_site_transient_browser_' . md5($_SERVER[ 'HTTP_USER_AGENT' ]), '__return_true');
}
